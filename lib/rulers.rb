require "rulers/version"
require "rulers/array"
require "rulers/funny"
require "rulers/routing"
require "rulers/util"
require "rulers/dependencies"
require "rulers/controller"
require "erubis"
require "rulers/file_model"

module Rulers
  class Application
    def call(env)
      if env['PATH_INFO'] == '/favicon.ico'
        return [404, {'Content-Type' => 'text/html'}, []]
      elsif env['PATH_INFO'] == '/'
        return [301, {'Content-Type' => 'text/html', 'Location' => '/quotes/a_quote'}, []]
      end
      klass, act = get_controller_and_action(env)
      controller = klass.new(env)
      begin
        text = controller.send(act)
      rescue
       return [500, {'Content-Type' => 'text/html'}, ["Something went wrong .... oopss.."]] 
      end
      [200, {'Content-Type' => 'text/html'}, [text]]
    end
  end

end
